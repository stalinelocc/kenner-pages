var gulp = require('gulp');
var uglify = require('gulp-uglify');
var babel = require('gulp-babel');
//var pipeline = require('readable-stream').pipeline;
var watch = require('gulp-watch');
var concat = require('gulp-concat');

gulp.task('compress', function () {
    return gulp.src('src/js/kenner-onde-comprar-2019.js')
            .pipe(babel({
                "presets": ["@babel/env"]
              }))
            .pipe(uglify())
            .pipe(gulp.dest('src/build/js'));
});

gulp.task('watch', function(done){
    //gulp.watch('src/scss/**/*.scss', {ignoreInitial: false}, gulp.series(['sass']) );
    gulp.watch('src/js/**/*.js', {ignoreInitial: false}, gulp.series(['compress']) );
    done();
});

exports.default = gulp.series(["watch"]);