var map,
    geocoder,
    image = { url: "/arquivos/kenner-map-marker.png", size: new google.maps.Size(71, 70), origin: new google.maps.Point(0, 0), anchor: new google.maps.Point(0, 70) },
    cameraStart = { lat: -22.962743, lng: -43.256632 },
    directionsService = new google.maps.DirectionsService(),
    directionsRenderer = new google.maps.DirectionsRenderer();geocoder = new google.maps.Geocoder(), map = new google.maps.Map(document.getElementById("k__map_stores"), { icon: image, center: cameraStart, zoom: 10 }), directionsRenderer.setMap(map);var marker,
    i,
    storeLocation = [['<div class="store"><p class="store-title">Kiosk Kenner Botafogo Praia Shopping <span class="store-type">Loja própria</span></p>   <div class="store-accordion"> <p class="store-address">Praia de Botafogo, 400 - 1º Pido - Botafogo - Rio de Janeiro - Rio de Janeiro</p> <span class="store-contact"><i></i>+55 (21) 98138-1444</span></div></div>', -22.9219385, -43.237562, 2], ['<div class="store"><p class="store-title">Kiosk Kenner Shopping Tijuca <span class="store-type">Loja própria</span></p>   <div class="store-accordion"> <p class="store-address">Av. Maracanã, 98 - 1º piso Quisque Q107 - Tijuca - Rio de Janeiro - Rio de Janeiro</p> <span class="store-contact"><i></i>+55 (21) 9 8529-1308</span></div></div>', -22.94752217, -43.1855209, 3], ['<div class="store"><p class="store-title">Kiosk Kenner Barra Shopping <span class="store-type">Loja própria</span></p>    <div class="store-accordion"> <p class="store-address">Av. das Américas, 4666 - 1º Piso - Nível Lagoa, Barra da Tijuca - Rio de Janeiro - Rio de Janeiro</p> <span class="store-contact"><i></i>+55 (21) 3395-0908</span></div></div>', -23.0033241, -43.3595124, 1], ['<div class="store"><p class="store-title">Kiosk Kenner Shopping Rio Sul <span class="store-type">Loja própria</span></p>  <div class="store-accordion"> <p class="store-address">Av. Lauro Sodré, 445 - 4º Piso - Botafogo - Rio de Janeiro - RJ</p> <span class="store-contact"><i></i>+55 (21) 98398-0110</span></div></div>', -22.9569798, -43.1790104, 4]],
    infowindow = new google.maps.InfoWindow();for (i = 0; i < storeLocation.length; i++) marker = new google.maps.Marker({ icon: image, position: new google.maps.LatLng(storeLocation[i][1], storeLocation[i][2]), map: map }), google.maps.event.addListener(marker, "click", function (e, o) {
  return function () {
    infowindow.setContent(storeLocation[o][0]), infowindow.open(map, e);
  };
}(marker, i));var userMarker,
    userInput = document.getElementById("searchTextField"),
    button = document.querySelector(".k__onde-comprar-search-icon"),
    searchBox = new google.maps.places.SearchBox(userInput),
    options = { types: ["(cities)"], componentRestrictions: { country: "br" } },
    autocomplete = new google.maps.places.Autocomplete(userInput, options),
    styleAlert = "color: #d00000; border: 1px solid #d00000; padding: 10px 20px;position: absolute; bottom: 10%; left: 50%; right: 50%; text-align: center;min-width: 400px; transform: translate(-50%, -50%); z-index: 500;";function codeAddress() {
  var e = userInput.value;geocoder.geocode({ address: e }, function (e, o) {
    if ("OK" == o) map.setCenter(e[0].geometry.location), userMarker = new google.maps.Marker({ map: map, position: e[0].geometry.location });else {
      var t = "Geolocalização falhou pela seguinte razão: " + o + ". <br/> verifique o local consultado";$(".k__geocode-alert").length || ($(".k__onde-comprar-content").append("<div class='k__geocode-alert' style='" + styleAlert + "'>" + t + "</div>"), setTimeout(function () {
        $(".k__geocode-alert").fadeOut().remove();
      }, 4e3));
    }
  }), calculateAndDisplayRoute(directionsService, directionsRenderer);
}function calculateAndDisplayRoute(s, t) {
  var e,
      n,
      r,
      a,
      i = [],
      c = userInput.value;function o() {
    i.sort(function (e, o) {
      return e[0] - o[0];
    }), s.route({ origin: c, destination: i[0][2], travelMode: "DRIVING" }, function (e, o) {
      "OK" === o ? t.setDirections(e) : console.log("Directions request failed due to " + o);
    }), function () {
      var e = document.querySelector("#result");e.innerHTML = "";for (var o = 0; o < storeLocation.length; o++) i[o][0] < 2e5 ? (e.innerHTML += i[o][1], $(".k__onde-comprar-menu #result .store")[o].setAttribute("data-path", i[o][2]), $(".store-not-found").hide(), t.setMap(map)) : ($(".store-not-found").show(), e.innerHTML = "", t.setMap(null));$(".search-info").html("<span class='search-info-result'>Resultados de busca: </br><strong>" + userInput.value + "</strong></span> <span class='search-info-num'>" + $("#result > .store").length + " Resultados</span>"), $(".k__onde-comprar-menu #result .store").on("click", function () {
        $(".k__onde-comprar-menu #result .store").removeClass("active"), $(this).toggleClass("active"), s.route({ origin: c, destination: $(this).attr("data-path"), travelMode: "DRIVING" }, function (e, o) {
          "OK" === o ? t.setDirections(e) : console.log("Directions request failed due to " + o);
        });
      }), $(".store-not-found svg").on("click", function () {
        $(".store-not-found").hide();
      });
    }();
  }!function () {
    for (var t = 0; t < storeLocation.length; t++) e = (storeLocation[t][1] + "," + storeLocation[t][2]).toString(), s.route({ origin: c, destination: e, travelMode: "DRIVING" }, function (e, o) {
      n = (storeLocation[t][1] + "," + storeLocation[t][2]).toString(), a = e.routes[0].legs[0].distance.value, e.routes[0].legs[0].distance.text, r = storeLocation[t][0], i.push([a, r, n, e]), "OK" === o || console.log("Directions request failed due to " + o);
    }), t == storeLocation.length - 1 && setTimeout(function () {
      o();
    }, 1e3);
  }();
}button.onclick = function () {
  codeAddress(), null != userMarker && userMarker.setMap(null);
}, onkeypress = function (e) {
  13 == (e.which || e.keyCode) && button.onclick();
};