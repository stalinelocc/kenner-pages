var map;
var geocoder;

/*Custom Marker*/
var image = {
    url: '/arquivos/kenner-map-marker.png',
    // This marker is 20 pixels wide by 32 pixels high.
    size: new google.maps.Size(71, 70),
    // The origin for this image is (0, 0).
    origin: new google.maps.Point(0, 0),
    // The anchor for this image is the base of the flagpole at (0, 32).
    anchor: new google.maps.Point(0, 70)
};

var cameraStart = {
    lat: -22.962743,
    lng: -43.256632
};

/*Main Map*/
var directionsService = new google.maps.DirectionsService();
var directionsRenderer = new google.maps.DirectionsRenderer();

geocoder = new google.maps.Geocoder();
// var latlng = new google.maps.LatLng(-34.397, 150.644);
// var mapOptions = {
//     zoom: 8,
//     center: latlng
// }
map = new google.maps.Map(document.getElementById('k__map_stores'), {
    icon: image,
    center: cameraStart,
    zoom: 10
});

directionsRenderer.setMap(map);

/*Stores*/
// var storeLocation = [
//     ['Barra Shopping </br> Kiosk', -23.0033241, -43.3595124, 1],
//     ['Shopping Tijuca </br> Kiosk', -22.9219385, -43.237562, 2],
//     ['Botafogo Praia Shopping </br> Kiosk', -22.94752217, -43.1855209, 3],
//     ['Shopping Rio Sul </br> Kiosk', -22.9569798, -43.1790104, 4]
// ];
var storeLocation = [['<div class="store"><p class="store-title">Kiosk Kenner Botafogo Praia Shopping <span class="store-type">Loja própria</span></p>   <div class="store-accordion"> <p class="store-address">Praia de Botafogo, 400 - 1º Pido - Botafogo - Rio de Janeiro - Rio de Janeiro</p> <span class="store-contact"><i></i>+55 (21) 98138-1444</span></div></div>', -22.9219385, -43.237562, 2], ['<div class="store"><p class="store-title">Kiosk Kenner Shopping Tijuca <span class="store-type">Loja própria</span></p>   <div class="store-accordion"> <p class="store-address">Av. Maracanã, 98 - 1º piso Quisque Q107 - Tijuca - Rio de Janeiro - Rio de Janeiro</p> <span class="store-contact"><i></i>+55 (21) 9 8529-1308</span></div></div>', -22.94752217, -43.1855209, 3], ['<div class="store"><p class="store-title">Kiosk Kenner Barra Shopping <span class="store-type">Loja própria</span></p>    <div class="store-accordion"> <p class="store-address">Av. das Américas, 4666 - 1º Piso - Nível Lagoa, Barra da Tijuca - Rio de Janeiro - Rio de Janeiro</p> <span class="store-contact"><i></i>+55 (21) 3395-0908</span></div></div>', -23.0033241, -43.3595124, 1], ['<div class="store"><p class="store-title">Kiosk Kenner Shopping Rio Sul <span class="store-type">Loja própria</span></p>  <div class="store-accordion"> <p class="store-address">Av. Lauro Sodré, 445 - 4º Piso - Botafogo - Rio de Janeiro - RJ</p> <span class="store-contact"><i></i>+55 (21) 98398-0110</span></div></div>', -22.9569798, -43.1790104, 4]];

var infowindow = new google.maps.InfoWindow();

var marker, i;

for (i = 0; i < storeLocation.length; i++) {
    marker = new google.maps.Marker({
        icon: image,
        position: new google.maps.LatLng(storeLocation[i][1], storeLocation[i][2]),
        map: map
    });

    google.maps.event.addListener(marker, 'click', function (marker, i) {
        return function () {
            infowindow.setContent(storeLocation[i][0]);
            infowindow.open(map, marker);
        };
    }(marker, i));
}

/*Search*/
//var newMarker = [];
var userInput = document.getElementById('searchTextField');
var button = document.querySelector('.k__onde-comprar-search-icon');
var searchBox = new google.maps.places.SearchBox(userInput);
var options = {
    types: ['(cities)'],
    componentRestrictions: { country: 'br' }
};

var autocomplete = new google.maps.places.Autocomplete(userInput, options);

//   google.maps.event.addListener(userInput, 'places_changed', function(){
//     var places = searchBox.getPlaces();

//     //bounds
//     var bounds = new google.maps.LatLngBounds();
//     var i, place;

//     for(i = 0; place = places[i]; i++){
//       console.log("geometry",place.geometry.location);
//       bounds.extend(place.geometry.location);
//     }

//     map.fitBounds(bounds);
//     map.setZoom(12);

//   });

//https://maps.google.com/maps/api/geocode/json?key=AIzaSyB-UfQFywrMfmJq0ZdgMufsuUbtt7ayP2k&address=73050-140&sensor=false

//   map.addListener('bounds_changed', function(){
//     console.log("bounds changed");
//     searchBox.setBounds(map.getBounds());
//   });

//   searchBox.addListener('places_changed', function(){
//     console.log("places changed");
//     var places = searchBox.getPlaces();

// //     if (places.length ===0 ){
// //       return;
// //     }

// //     var bounds = new google.maps.LatLngBounds();
// //     places.forEach(function(p){
// //       if(!p.geometry){
// //         return;
// //       }
// //       newMarker.push(new google.maps.Marker);
// //     });  

//   });
var styleAlert = "color: #d00000; border: 1px solid #d00000; padding: 10px 20px;" + "position: absolute; bottom: 10%; left: 50%; right: 50%; text-align: center;" + "min-width: 400px; transform: translate(-50%, -50%); z-index: 500;";

var userMarker;
function codeAddress() {
    var address = userInput.value;

    geocoder.geocode({
        'address': address
    }, function (results, status) {
        if (status == 'OK') {
            map.setCenter(results[0].geometry.location);

            userMarker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
        } else {
            //alert('Geolocalização falhou pela seguinte razão: ' + status);
            var msg = "Geolocalização falhou pela seguinte razão: " + status + ". <br/> verifique o local consultado";

            if (!$(".k__geocode-alert").length) {
                $(".k__onde-comprar-content").append("<div class='k__geocode-alert' style='" + styleAlert + "'>" + msg + "</div>");
                setTimeout(function () {
                    $(".k__geocode-alert").fadeOut().remove();
                }, 4000);
            }
        }
    });
    calculateAndDisplayRoute(directionsService, directionsRenderer);
}

/**/
// function calcRoute() {
//     var selectedMode = document.getElementById('mode').value;
//     var request = {
//         origin: haight,
//         destination: oceanBeach,
//         // Note that JavaScript allows us to access the constant
//         // using square brackets and a string value as its
//         // "property."
//         travelMode: google.maps.TravelMode[selectedMode]
//     };
//     directionsService.route(request, function (response, status) {
//         if (status == 'OK') {
//             directionsRenderer.setDirections(response);
//         }
//     });
// }

function calculateAndDisplayRoute(directionsService, directionsRenderer) {
    var groupResponse = [];

    var start = userInput.value;
    var end;
    var endArr;
    var storeAddress;
    var killometerInteger;
    var killometerInfo;
    /* Multiple distance Analysis from store array*/

    function analysePath() {
        /* Run several times to make a list of addresses */
        for (let index = 0; index < storeLocation.length; index++) {
            end = (storeLocation[index][1] + "," + storeLocation[index][2]).toString();
            directionsService.route({
                origin: start,
                destination: end,
                travelMode: 'DRIVING'
            }, function (response, status) {
                //var killometerInfo = response.routes[0].legs[0].distance.text.split(" ")[0];
                endArr = (storeLocation[index][1] + "," + storeLocation[index][2]).toString();
                killometerInteger = response.routes[0].legs[0].distance.value;
                killometerInfo = response.routes[0].legs[0].distance.text;
                storeAddress = storeLocation[index][0];
                groupResponse.push([killometerInteger, storeAddress, endArr, response]);

                if (status === 'OK') {
                    /* Closest path will render in final path */
                    //directionsRenderer.setDirections(response);
                } else {
                    console.log('Directions request failed due to ' + status);
                }
            });
            if (index == storeLocation.length - 1) {
                setTimeout(function () {
                    responseFinalPath();
                }, 1000);
            }
        }
    }
    analysePath();

    function responseFinalPath() {
        /* Sort by killometer */
        groupResponse.sort(function (a, b) {
            return a[0] - b[0];
        });
        /* Show first in the list */
        directionsService.route({
            origin: start,
            destination: groupResponse[0][2],
            travelMode: 'DRIVING'
        }, function (response, status) {
            if (status === 'OK') {
                directionsRenderer.setDirections(response);
            } else {
                console.log('Directions request failed due to ' + status);
            }
        });
        printPathResult();
    }

    function printPathResult() {
        var showResult = document.querySelector("#result");
        //clear previous
        showResult.innerHTML = "";
        var KMLimitToShow = 200000;

        for (let index = 0; index < storeLocation.length; index++) {
            /* Print results if under KM is under a number X */
            if (groupResponse[index][0] < KMLimitToShow) {
                showResult.innerHTML += groupResponse[index][1];
                $(".k__onde-comprar-menu #result .store")[index].setAttribute("data-path", groupResponse[index][2]);
                $(".store-not-found").hide();
                /* recall render if cleared */
                directionsRenderer.setMap(map);
            } else {
                /* Show error */
                $(".store-not-found").show();
                /* Clear previous */
                showResult.innerHTML = "";
                /* clear rendered */
                directionsRenderer.setMap(null);
            }
        }
        $(".search-info").html("<span class='search-info-result'>Resultados de busca: </br><strong>" + userInput.value + "</strong>" + "</span> <span class='search-info-num'>" + $("#result > .store").length + ' Resultados' + "</span>");

        /* Click display path */
        $(".k__onde-comprar-menu #result .store").on("click", function () {
            //clear previous
            $(".k__onde-comprar-menu #result .store").removeClass("active");
            $(this).toggleClass("active");

            /*Re-render clicked path */
            directionsService.route({
                origin: start,
                destination: $(this).attr("data-path"),
                travelMode: 'DRIVING'
            }, function (response, status) {
                if (status === 'OK') {
                    directionsRenderer.setDirections(response);
                } else {
                    console.log('Directions request failed due to ' + status);
                }
            });
        });

        /* Click to close not-found */
        $(".store-not-found svg").on("click", function () {
            $(".store-not-found").hide();
        });
    }
}

button.onclick = function () {
    codeAddress();
    if (userMarker != undefined) {
        /*Clear previous markers after defined*/
        userMarker.setMap(null);
    }
};
onkeypress = function (event) {
    var key = event.which || event.keyCode;
    if (key == 13) {
        //Trigger input button when Enter
        button.onclick();
    }

    // if(userInput.value.length >= 0){
    //     button.style.backgroundImage = "url(/arquivos/search-clear.png)";
    //     button.style.backgroundPosition = "0";
    // } else {
    //     button.style.backgroundImage = "url(/arquivos/icons-s679911c6c9.png)";
    //     button.style.backgroundPosition = "0 -1579px";
    // }
};