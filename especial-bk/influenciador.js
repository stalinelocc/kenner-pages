
    $(".ler").click(function(){
        $(".text-kenner").toggle();
        $(".hot-list").toggle();  
        if($(".ler").text() == "Continuar Lendo"){
            $(this).text("X Fechar");
        }else{
            $(this).text("Continuar Lendo");
        }           
        $(".act-fechar").toggle();
        $(".act-abrir").toggle();  
    });
   
    $(".act-abrir").click(function(){ //abrir atributos da linha 
        $(".hot-list").toggle();  
        $(".act-abrir").toggle();  
        $(".act-fechar").toggle();  // fechar atributos da linha
        $(".influenciador-attributes").toggleClass("influenciador-1280");
        $(".influenciador-product img").toggleClass("influenciador-img-low");
        $(".influenciador-product").toggleClass("influenciador-product-low");
        $('.texto-desktop').toggle();
        $('.ler').toggle();
    });

    $(".act-fechar").click(function(){
        $(".hot-list").toggle();            
        $(".act-abrir").toggle();              
        $(".act-fechar").toggle();
        $(".influenciador-attributes").toggleClass("influenciador-1280");
        $(".influenciador-product img").toggleClass("influenciador-img-low");
        $(".influenciador-product").toggleClass("influenciador-product-low");
        $('.texto-desktop').toggle();
        $('.ler').toggle();
    });


    $(".ler-mobile").click(function(){         
        $(".texto-mobile").toggle();
        $(this).html($(this).html()=="Ler Manifesto"?"X Fechar":"Ler Manifesto");
    });    

    $(".act-abrir-mobile").click(function(){ //abrir atributos da linha
        $(".hot-list").toggle();  
        $(".act-abrir-mobile").toggle();  
        $(".act-fechar-mobile").toggle();
    });

    $(".act-fechar-mobile").click(function(){ //abrir atributos da linha
        $(".hot-list").toggle();  
        $(".act-abrir-mobile").toggle();  
        $(".act-fechar-mobile").toggle();
    });