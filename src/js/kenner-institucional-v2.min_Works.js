var KennerInstitucional = {
    config: {},
    methods: {
        _controlItens: function() {
            $(".kenner__title").on("click", function() {
                var e = $(this).parent().hasClass("active");
                $(".kenner__institucional--item").removeClass("active"),
                e ? ($(this).parent().removeClass("active"),
                $("body").removeClass("active__item")) : ($(this).parent().addClass("active"),
                $("body").addClass("active__item"))
            }),
            $(".kenner__subTitle").on("click", function() {
                var e = $(this).parent().hasClass("active-sub");
                $(".kenner__institucional--sub-item").removeClass("active-sub"),
                e ? $(this).parent().removeClass("active-sub") : $(this).parent().addClass("active-sub")
            })
        },
        _controlsInstitucional2Tabs: function() {
            $(".kenner__content--item h3").on("click", function() {
                var e = !!$(this).parent().hasClass("active");
                $(".kenner__content--item").removeClass("active"),
                e ? $(this).parent().removeClass("active") : $(this).parent().addClass("active")
            })
        },
        _validateEmail: function(e) {
            return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(String(e).toLowerCase())
        },
        _validaCNPJ: function(e) {
            // if ("" == (e = e.replace(/[^\d]+/g, "")))
            //     return !1;
            // if (14 != e.length)
            //     return !1;
            // if ("00000000000000" == e || "11111111111111" == e || "22222222222222" == e || "33333333333333" == e || "44444444444444" == e || "55555555555555" == e || "66666666666666" == e || "77777777777777" == e || "88888888888888" == e || "99999999999999" == e)
            //     return !1;
            // if("[0-9]{2}\.?[0-9]{3}\.?[0-9]{3}\/?[0-9]{4}\-?[0-9]{2}".test(String(e).toLowerCase())){
            //     return !1;
            // }    
            // for (tamanho = e.length - 2,
            // numeros = e.substring(0, tamanho),
            // digitos = e.substring(tamanho),
            // soma = 0,
            // pos = tamanho - 7,
            // i = tamanho; 1 <= i; i--)
            //     soma += numeros.charAt(tamanho - i) * pos--,
            //     pos < 2 && (pos = 9);
            // if (resultado = soma % 11 < 2 ? 0 : 11 - soma % 11,
            // resultado != digitos.charAt(0))
            //     return !1;
            // for (tamanho += 1,
            // numeros = e.substring(0, tamanho),
            // soma = 0,
            // pos = tamanho - 7,
            // i = tamanho; 1 <= i; i--)
            //     soma += numeros.charAt(tamanho - i) * pos--,
            //     pos < 2 && (pos = 9);
            // return resultado = soma % 11 < 2 ? 0 : 11 - soma % 11,
            // resultado == digitos.charAt(1)


            /* Versão 18.11.2019*/
            if( /[0-9]{2}\.?[0-9]{3}\.?[0-9]{3}\/?[0-9]{4}\-?[0-9]{2}/g.test(String(e).toLowerCase()) && e.length <= 18){
                for (tamanho = e.length - 2,
                numeros = e.substring(0, tamanho),
                digitos = e.substring(tamanho),
                soma = 0,
                pos = tamanho - 7,
                i = tamanho; 1 <= i; i--)
                    soma += numeros.charAt(tamanho - i) * pos--,
                    pos < 2 && (pos = 9);
                if (resultado = soma % 11 < 2 ? 0 : 11 - soma % 11,
                resultado != digitos.charAt(0))
                    return !1;
                for (tamanho += 1,
                numeros = e.substring(0, tamanho),
                soma = 0,
                pos = tamanho - 7,
                i = tamanho; 1 <= i; i--)
                    soma += numeros.charAt(tamanho - i) * pos--,
                    pos < 2 && (pos = 9);
                return resultado = soma % 11 < 2 ? 0 : 11 - soma % 11,
                resultado == digitos.charAt(1)
            } else {
                return false;
            }
        },
        _passedImage: "",
        _nomeImage: "",
        _sendMasterDataRevendedor: function() {
            $(".form-revendedor input").on("focus", function() {
                0 < $(".email").find(".error-msg-email").length && $(".error-msg-email").remove(),
                0 < $(".cnpj").find(".error-msg-cnpj").length && $(".error-msg-cnpj").remove(),
                $(this).parent().removeClass("msg-error-js")
            }),
            $('#imagem').on('change', function(e) {      
                /*Monta imagem quando usuário seleciona*/
                // var file = e.target.files[0]; 
                var file = e.target.files; 
                console.log("#imagem nome", e.target.files)
                var fileName = e.target[0].files[0].name;
                var fileData = new FormData();
                $.each(file, function(key, value){
                    // fileData[key] = value;
                    fileData.append(key, value);
                });
                // KennerInstitucional.methods._passedImage = URL.createObjectURL(file);
                KennerInstitucional.methods._nomeImage = fileName
                KennerInstitucional.methods._passedImage = fileData;
            }),     

            $('.form-revendedor input[type="submit"]').on("click", function(e) {
                e.preventDefault();
                var o = 0
                  , n = $(".form-revendedor select").val();
                function sendDocument(idDoc){
                    urlimage = 'https://kenner.vtexcrm.com.br/DynamicForm/GetFile?dataEntityInstanceId=RV-'+idDoc+'&fileName='
                    +KennerInstitucional.methods._nomeImage
                    $.ajax({
                        data: KennerInstitucional.methods._passedImage,
                        enctype: 'multipart/form-data',
                        'headers': {
                            'Accept': 'application/vnd.vtex.ds.v10+json',
                            //'Content-Type': 'application/json'
                        },
                        url: '/api/dataentities/RV/documents/'+idDoc+'/imagem/attachments',
                        type: 'POST',
                        processData: false,
                        contentType: false,
                        enctype: 'multipart/form-data',
                        beforeSend: function (request){
                            request.setRequestHeader("Accept", "application/vnd.vtex.ds.v10+json");
                        }
                    }).done(function(e) {            
                        console.log('Anexado com sucesso: ', e);
                        updateDocument(urlimage, idDoc)
                    });
                };

                function updateDocument(url, id){
                    var                                 
                    fields = {
                        'txtimagem': url
                    };
                    
                    $.ajax({
                        'headers': {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        'url': '/api/dataentities/RV/documents/'+id,
                        'async': false,
                        'crossDomain': true,
                        'type': 'PUT',          
                        'data': JSON.stringify(fields)
                    }).success(function (data) {
                        console.log('nome da image cadastrado com sucesso');
                        
                    }).fail(function (data) {                       
                        alert('Não foi possível enviar sua mensagem');
                    });
                };

                if (Array.from($(".form-revendedor input"), function(e, n) {
                    var a = $(e).val();
                    "" === a || null == a ? (o++,
                    $(e).parent().addClass("msg-error-js")) : $(e).parent().removeClass("msg-error-js")
                }),
                "" === n || null == n ? (o++,
                $(".form-revendedor select").parent().addClass("msg-error-js")) : $(".form-revendedor select").parent().removeClass("msg-error-js"),
                $(".error-msg-email").remove(),
                !1 === KennerInstitucional.methods._validateEmail($('.kenner__form--item input[type="email"]').val()) && ($('.form-revendedor input[type="email"]').parent().append('<small class="error-msg-email" style="display: inline-block; font-size: 13px; margin-top: -20px; background: #FF0000; border-radius: 30px; color: white; float: right; padding:2px 15px;">Informe um e-mail válido!</small>'),
                $('.form-revendedor input[type="email"]').parent().addClass("msg-error-js")),
                !1 === KennerInstitucional.methods._validaCNPJ($('.kenner__form--item input[name="cnpj"]').val()) && ($('.form-revendedor input[name="cnpj"]').parent().append('<small class="error-msg-cnpj" style="display: inline-block; font-size: 13px; margin-top: -20px; background: #FF0000; border-radius: 30px; color: white; float: right; padding:2px 15px;">Informe um CNPJ válido!</small>'),
                $('.form-revendedor input[name="cnpj"]').parent().addClass("msg-error-js")),

                KennerInstitucional.methods._validateEmail($('.kenner__form--item input[type="email"]').val()) && 0 === o) { 
                    /* Cria Documento */
                    var a = {
                        cnpj: $('.form-revendedor input[name="cnpj"]').val(),
                        razaoSocial: $('.form-revendedor input[name="razaoSocial"]').val(),
                        nomeFantasia: $('.form-revendedor input[name="nomeFantasia"]').val(),
                        nomeDoContato: $('.form-revendedor input[name="nomeDoContato"]').val(),
                        email: $('.form-revendedor input[name="email"]').val(),
                        endereco: $('.form-revendedor input[name="endereco"]').val(),
                        complemento: $('.form-revendedor input[name="complemento"]').val(),
                        bairro: $('.form-revendedor input[name="bairro"]').val(),
                        estado: $('.form-revendedor select[name="estado"]').val(),
                        cidade: $('.form-revendedor input[name="cidade"]').val(),
                        telefone: $('.form-revendedor input[name="telefone"]').val(),
                        quantasLojasPossui: $('.form-revendedor input[name="quantasLojasPossui"]').val(),
                        imagem: ''
                        // imagem: $('.form-revendedor input[name="imagem"]').val(),
                        // imagem: '<img src='+$('.form-revendedor input[name="imagem"]').val()+ '/>',
                        // imagem: KennerInstitucional.methods._passedImage
                    }, t = JSON.stringify(a);
                    console.log(t),

                    $.ajax({
                        url: "/api/dataentities/RV/documents",
                        type: "POST",
                        data: t,
                        dataType: "json",
                        crossDomain: true,
                        async: false,
                        headers: {
                            Accept: "application/vnd.vtex.ds.v10+json",
                            "Content-Type": "application/json"
                        }
                    }).done(function(e) {
                        console.log(e),
                        feedbackHtml = '<p class="form--title--show feedback-success">Informações enviadas com sucesso!<br/><span>Em breve lhe retornaremos.</span></p>',
                        $(".form-revendedor").html(feedbackHtml);
                        setTimeout(function(){
                            sendDocument(e.DocumentId);
                        }, 100);
                    }).fail(function(e) {
                        console.log("falha", e);
                        feedbackHtml = '<p class="form--title--show feedback-error">Desculpe o transtorno!<br/><span>Ocorreu algum erro no processamento.</span></p>',
                        $(".form-revendedor").html(feedbackHtml)
                    })
                }
            })
        },
        _onLoadPage: function() {
            href = -1 < window.location.hash.indexOf("#") ? window.location.hash.replace("#", "") : window.location.hash,
            $("body").addClass("active__item"),
            $(".kenner__institucional--item").removeClass("active"),
            $("." + href).addClass("active")
        },
        init: function() {
            console.log("Oiiii!!!"),
            this._controlItens(),
            this._controlsInstitucional2Tabs(),
            this._sendMasterDataRevendedor(),
            this._onLoadPage(),
            768 < $(window).width() & "" === window.location.hash && ($(".kenner__institucional--item").eq(0).addClass("active"),
            $(".kenner__content--item").eq(0).addClass("active"))
        },
        init_ajax: function() {}
    },
    ajax: function() {
        return this.methods.init_ajax()
    },
    mounted: function() {
        return this.methods.init()
    }
};
$(document).ready(function() {
    KennerInstitucional.mounted()
}),
$(document).ajaxStop(function() {
    KennerInstitucional.ajax()
});
