function loadMap(){
    var loader = '<div class="lds-ripple"><div></div><div></div></div>';
    $("body.onde-comprar").prepend(loader);


    var map;
    var geocoder;
 
    var storeLocation = [];

    /*Custom Marker*/
    var markerImage = {
        // url: '/arquivos/kenner-map-marker.png',
        url: '/arquivos/kenner-map-marker_sz2.png',
        // This marker is 20 pixels wide by 32 pixels high.
        // size: new google.maps.Size(71,70),
        size: new google.maps.Size(20, 27),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        // anchor: new google.maps.Point(0,70)
        anchor: new google.maps.Point(10, 25)
    };


    var cameraStart = {
        lat: -22.962743,
        lng: -43.256632
    };

    /*Main Map*/
    var directionsService = new google.maps.DirectionsService();
    var directionsRenderer = new google.maps.DirectionsRenderer({preserveViewport: true, suppressMarkers: true});


    geocoder = new google.maps.Geocoder();
    // var latlng = new google.maps.LatLng(-34.397, 150.644);
    // var mapOptions = {
    //     zoom: 8,
    //     center: latlng
    // }
    map = new google.maps.Map(document.getElementById('k__map_stores'),{
        icon: markerImage,
        center: cameraStart,
        zoom: 12
    });

    //window.globals for test purpose only
    window.directionsService = directionsService;
    window.directionsRenderer = directionsRenderer;
    window.map = map;

    function adjustMapBy(x, y, scopeMap){
        /* Adjust map in desktop */
        var windowWidth = $(window).width();
        map = scopeMap || map;
        x = (x) || -(windowWidth / 4.5);
        y = (y) || 0;
        if(windowWidth > 768){
            //Desktop
            map.Animation = false;
            map.panBy(x, y);
            map.zoom = 12;
             //Auto adjust Pan screen
            directionsRenderer.preserveViewport = true;
        } else {
            //Mobile
            directionsRenderer.preserveViewport = false;
            map.zoom = 14;
            map.panBy(0, 0);
        }
    }

    directionsRenderer.setMap(map);
    adjustMapBy();

    
    $(window).resize(function(){
        var windowWidth = $(window).width();
        if(windowWidth > 768){
            //Desktop
            directionsRenderer.preserveViewport = true;
            map.zoom = 11;
        } else {
            //Mobile
            directionsRenderer.preserveViewport = false;
            map.zoom = 13;
        }
    });

    /*Stores*/
    // var storeLocation = [
    //     ['Barra Shopping </br> Kiosk', -23.0033241, -43.3595124, 1],
    //     ['Shopping Tijuca </br> Kiosk', -22.9219385, -43.237562, 2],
    //     ['Botafogo Praia Shopping </br> Kiosk', -22.94752217, -43.1855209, 3],
    //     ['Shopping Rio Sul </br> Kiosk', -22.9569798, -43.1790104, 4]
    // ];

    /* var storeLocation_v1 = [
        ['<div class="store"><p class="store-title">Kiosk Kenner Botafogo Praia Shopping <span class="store-type">Loja própria</span></p>   <div class="store-accordion"> <p class="store-address">Praia de Botafogo, 400 - 1º Piso - Botafogo - Rio de Janeiro - Rio de Janeiro</p> <a href="tel:5521981381444" class="store-contact"><i></i>(21) 98138-1444</a> <div class="store-info"><span class="store-see-in-map">Ver no mapa</span><a href="tel:5521981381444" class="store-call">Ligar</a> <span class="store-details">Detalhes da loja</span> </div> </div></div> ', -22.9219385, -43.237562, 2],
        ['<div class="store"><p class="store-title">Kiosk Kenner Shopping Tijuca <span class="store-type">Loja própria</span></p>   <div class="store-accordion"> <p class="store-address">Av. Maracanã, 98 - 1º piso Quisque Q107 - Tijuca - Rio de Janeiro - Rio de Janeiro</p> <a href="tel:5521985291308" class="store-contact"><i></i>(21) 9 8529-1308</a> <div class="store-info"><span class="store-see-in-map">Ver no mapa</span><a href="tel:5521985291308" class="store-call">Ligar</a> <span class="store-details">Detalhes da loja</span> </div> </div></div>', -22.94752217, -43.1855209, 3],
        ['<div class="store"><p class="store-title">Kiosk Kenner Barra Shopping <span class="store-type">Loja própria</span></p>    <div class="store-accordion"> <p class="store-address">Av. das Américas, 4666 - 1º Piso - Nível Lagoa, Barra da Tijuca - Rio de Janeiro - Rio de Janeiro</p> <a href="tel:552133950908" class="store-contact"><i></i>(21) 3395-0908</a> <div class="store-info"><span class="store-see-in-map">Ver no mapa</span><a href="tel:552133950908" class="store-call">Ligar</a> <span class="store-details">Detalhes da loja</span> </div> </div></div>', -23.0033241, -43.3595124, 1],
        ['<div class="store"><p class="store-title">Kiosk Kenner Shopping Rio Sul <span class="store-type">Loja própria</span></p>  <div class="store-accordion"> <p class="store-address">Av. Lauro Sodré, 445 - 4º Piso - Botafogo - Rio de Janeiro - RJ</p> <a href="tel:21983980110" class="store-contact"><i></i>(21) 98398-0110</a> <div class="store-info"><span class="store-see-in-map">Ver no mapa</span><a href="tel:5521983980110" class="store-call">Ligar</a> <span class="store-details">Detalhes da loja</span> </div> </div></div>', -22.9569798, -43.1790104, 4]
    ]; */

    $.ajax({
        "url": "/api/dataentities/OC/search?_fields=Nome,Tipo,Endereco,Contato,lat,lng",
        "method":"GET",
        "X-VTEX-API-AppKey": "vtexappkey-kenner-ETWXKY",
        "X-VTEX-API-AppToken":  "DFYRKOVVEGYTIFMFOQTDQNYIPXMDSKKLAHKFYKMMGDZELHDYJPDKWUSQHQCNCRCUWMSAUHHDDGWNNUXHTNUDGAPGOUXIIZJOAEHZPCEBIZHQXECNTUTPWZOWXMLAJZVD",
        "headers": {
            'Accept':  'application/json',
            'Content-Type': 'application/json'
        },
        success: function(data){        
            distributeAllMarkers(data);        
        },
        error: function(e){
            console.log("Não foi possível consultar a base de lojas", e);
        },
        complete: function(){
            $(".lds-ripple").show();
        }
    });


    var infowindow = new google.maps.InfoWindow();

    var marker, i, userMarker, searchTypeGeo;
    //var newMarker = [];
    var userInput = document.getElementById('searchTextField');
    var button = document.querySelector('.k__onde-comprar-search-icon');
    var searchBox = new google.maps.places.SearchBox(userInput);
    var autocomplete = new google.maps.places.Autocomplete(userInput,options);
    
    var options = {
        types: ['(cities)'],
        componentRestrictions: {
            country: 'br'
        }
    };


    function distributeAllMarkers(data) {
        storeLocation = data;
        
        for (i = 0; i < storeLocation.length; i++) {
            //lat+lng for every store
            marker = new google.maps.Marker({
                icon: markerImage,
                position: new google.maps.LatLng(storeLocation[i].lat, storeLocation[i].lng),
                map: map,
                title: "",
            });

            //Stores Names infowindow
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(storeLocation[i].Nome);//lat+lng for every store
                    infowindow.open(map, marker);
                }
            }
            )(marker, i));
        }

        loadGeoLocation(data);
    }
    //test

    function mapAlert(msg) {
        var styleAlert = "background: #fff; color: #d00000; border: 1px solid #d00000; padding: 20px 20px;" 
                            + "position: absolute; bottom: 10%; left: 50%; right: 50%; text-align: center;" 
                            + "min-width: 400px; min-height: 40px; transform: translate(-50%, -50%); z-index: 500;";

        msg = msg || 'error';
        if (!$(".k__geocode-alert").length) {
            $(".k__onde-comprar-content").append("<div class='k__geocode-alert' style='" + styleAlert + "'>" + msg + "</div>");
            setTimeout(function() {
                $(".k__geocode-alert").fadeOut().remove();
            }, 6000);
        }
    }

    infoWindow = new google.maps.InfoWindow;

    var userLocation;
    function getUserLocation(type) {
        if(type){
            console.log("Event type: ", type);
            codeAddress(null, type);
            return;
        }
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                userLocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                infoWindow.setPosition(userLocation);               
                infoWindow.setContent('Você.');
                infoWindow.open(map);
                map.setCenter(userLocation);
    //          map = new google.maps.Map(document.getElementById('k__map_stores'),{
    //              icon: markerImage,
    //              center: userLocation,
    //              zoom: 6
    //          });
            console.log("UserLocation setted", userLocation)
            codeAddress(userLocation, "userLocation"); 
            return
            }, function() {
                //handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }

        codeAddress(null , "empty");   
    }

    // getUserLocation();


    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ? 'Error: The Geolocation service failed.' : 'Error: Your browser doesn\'t support geolocation.');
        adjustMapBy();
        infoWindow.open(map);
    }

    function codeAddress(userLocation, type) {    
        $(".lds-ripple").show(); 
        /*close error if new search*/
        $(".store-not-found").hide();

        //Reset Viewport to auto-adjust again
//         directionsRenderer.preserveViewport = false;

        var address;

        if(type == "inputType"){
            address = userInput.value;
        }
        
        if(type == "userLocation" && userLocation.lat){
            address = (userLocation.lat + ", " + userLocation.lng).toString();
        }
        
        if(type == "empty"){
            console.log("Suggested default address: ", type);
            address = "Rio de Janeiro";
            $(".store-not-found").show();
            map.zoom = 10;
            userMarker = null;
            calculateAndDisplayRoute(directionsService, directionsRenderer, type);
            return;
        }
     
        geocoder.geocode({
            'address': address
        }, function(results, status) {

            if (status == 'OK') {
                map.setCenter(results[0].geometry.location);
                userMarker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
            } else {
                //alert('Geolocalização falhou pela seguinte razão: ' + status);
                var msg = "Geolocalização falhou pela seguinte razão: " + status + ". <br/> verifique o local consultado";
                mapAlert(msg);
            }
        });
        calculateAndDisplayRoute(directionsService, directionsRenderer, type);
    }


    function calculateAndDisplayRoute(directionsService, directionsRenderer, type) {
        var groupResponse = [];
        var start =  userInput.value || (userLocation != undefined ? (userLocation.lat + ", " + userLocation.lng).toString() : "Rio de Janeiro");
        var end;
        var killometerInteger;
        var killometerInfo;
        let index = 0;

        if(type == "inputType"){            
            start = userInput.value;
        }

        if(type == "userLocation" && userLocation.lat){
            start = (userLocation.lat + ", " + userLocation.lng).toString();
        }

        if(type == "empty"){
            start = "Rio de Janeiro";
        }
        console.log("Analyzing for ", type, " path");

        /* Multiple distance Analysis from store array*/
        function analyzingPaths() {
            /* Run several times to make a list of addresses */    

            end = (storeLocation[index].lat + "," + storeLocation[index].lng).toString();
            directionsService.route({
                origin: start,
                destination: end,
                travelMode: 'DRIVING'
            }, function(response, status) {
                endArr = (storeLocation[index].lat + "," + storeLocation[index].lng).toString();
                killometerInteger = response.routes[0].legs[0].distance.value;
                killometerInfo = response.routes[0].legs[0].distance.text;

                var infoCombined = $.extend({killometerInteger}, storeLocation[index], {response});

                groupResponse.push(infoCombined);

                if (status === 'OK') {
                    /* Closest path will render in final path */
                    //directionsRenderer.setDirections(response);
                } else {
                    console.log('Directions request failed due to ' + status);
                    mapAlert('Directions request failed due to ' + status);
                }

                index++;

                if(index == storeLocation.length){
                    //Default search
                    if(userLocation != undefined && searchTypeGeo){
                        $(".k__onde-comprar").attr("id", "screen__user-geolocation");
                    }
                    if (!searchTypeGeo) {
                        //user search
                        $(".k__onde-comprar").attr("id", "screen__user-search");
                    }
                    responseFinalPath(type);
                }

                if(index < storeLocation.length){
                    /* Recursive call Async */
                    analyzingPaths();
                    directionsRenderer.setMap(map);
                }
            });
        }

        analyzingPaths();

        function responseFinalPath(type) {
            /* Sort by killometer */
            groupResponse.sort(function(a, b) {
                return a.killometerInteger - b.killometerInteger;
            });


            if(type != "empty"){
                /* Only show direction if not empty*/
                  /* Final step render FIRST path in the list */
                directionsService.route({
                    origin: start,
                    destination: groupResponse[0].lat + "," + groupResponse[0].lng ,
                    travelMode: 'DRIVING'
                }, function(response, status) {
                    if (status === 'OK') {
                        console.log("directionsService", status);
                        directionsRenderer.setDirections(response);                        
                        adjustMapBy();
                    } else {
                        console.log('Directions request failed due to ' + status);
                        mapAlert('Directions request failed due to ' + status);
                    }
                });
            }

            // Print ordered HTML response
            buildOrderedStores(groupResponse);
            $(".search-info-result strong").text(userInput.value);
            $(".search-info-num").text($("#result > .store").length + ' Resultados');
        }


        function buildOrderedStores(groupResponse){
            var concatStores = "";
            var phone = "";
            var KM_LIMIT = 200000;
                        
            //Clear previous result
            $("BODY #result").children().remove();

            groupResponse && groupResponse.map((item, index, array)=>{

                phone = item.Contato && item.Contato.replace(" - ","").replace("(", "").replace(")", "").replace(" ", "");
                if (item.killometerInteger < KM_LIMIT) {
                concatStores += `
                            <div class="store" data-path="${item.lat && item.lat}, ${item.lng && item.lng}">
                                <p class="store-title">${item.Nome && item.Nome}  <span class="store-type">${item.Tipo && item.Tipo}</span></p>
                                <div class="store-accordion"> 
                                    <p class="store-address">${item.Endereco && item.Endereco}</p>                                   
                                        <a href="tel:${phone && phone}" class="store-contact">
                                            <i></i>${item.Contato && item.Contato}
                                        </a> 
                                        <div class="store-info">
                                            <span class="store-see-in-map">Ver no mapa</span>
                                            <a href="tel:${phone && phone}" class="store-call">Ligar</a> <span class="store-details">Detalhes da loja</span> 
                                        </div> 
                                    </div>
                                </div>`;

                /* recall render if cleared */
                index == array.length - 1
                    && $("BODY #result").append(concatStores)
                    && $(".store-not-found").hide();
                } else {
                    /* Show error */
                    $(".k__onde-comprar").attr("id", "screen__not-found");
                    $(".store-not-found").show();
                    /* Clear previous */
                    $("BODY #result").children().remove();
                    $(".lds-ripple").hide();
                    /* clear rendered */
                    directionsRenderer.setMap(null);
                }
            });

            $(".lds-ripple").hide();

            /*see in map button  */
            if (!$(".search-info").length) {
                $(".k__onde-comprar-search").append("<div class='search-info'> <span class='search-info-result'>Resultados de busca: </br><strong>" + userInput.value + "</strong>" + "</span> <span class='search-info-num'>" + $("#result > .store").length + ' Resultados' + "</span></div>");
            }

            if (!$(".go-back-container").length) {
                !$(".go-back").length 
                    && $("<div class='go-back-container'><span class='go-back'>Voltar</span> <span class='store-name'></span></div>").insertBefore(".k__onde-comprar-bar");
                $(".lds-ripple").hide();
            }

            $(".store-see-in-map").on("click", function() {
                $(".k__onde-comprar").attr("id", "screen__see-in-map");
            });

            /*See all stores in map (Mobile) */
            $(".see-in").on("click", function(event) {
                event.stopPropagation();
                $(".k__onde-comprar").attr("id", "screen__see-all-in-map");
            });

            $(".store-details").each(function() {
                var storeName = $(this).parents(".store").find(".store-title").html();
                $(".store-name").html(storeName);
            });

            $(".store-details").on("click", function(event) {
                event.stopPropagation();
                $(".k__onde-comprar").attr("id", "screen__store-details");
                var storeName = $(this).parents(".store").find(".store-title").html();
                $(".store-name").html(storeName);
            });
        
            /* Click display path */
            var flag = true;
                    
            /* View desktop correction */
            
            $(".k__onde-comprar-menu #result .store").on("click", function(event) {
                event.stopPropagation();
            
                //clear previous
                $(".k__onde-comprar-menu #result .store").not(this).removeClass("active");
                //add current
                $(this).toggleClass("active");

                /*Re-render clicked path */

                directionsService.route({
                    origin: start,
                    destination: $(this).attr("data-path"),
                    travelMode: 'DRIVING'
                }, function(response, status) {
                    if (status === 'OK') {
                        console.log("directionsService", status);
                        directionsRenderer.setDirections(response);
//                         setTimeout(function(){
//                             directionsRenderer.panBy(-300, 0);
//                             directionsRenderer.map.zoom = 10;
//                         }, 100);                            
                    } else {
                        console.log('Directions request failed due to ' + status);
                        mapAlert('Directions request failed due to ' + status);
                    }
                });
            });

        }
    }


    /*First load store suggestion */
    function loadGeoLocation(data) {
        searchTypeGeo = true;
        setTimeout(function() {            
            getUserLocation();
            $(".k__onde-comprar").attr("id", "screen__default");
        }, 1000);
    }

        
    /* Click to close not-found */
    $(".store-not-found svg").on("click", function(event) {
        event.stopPropagation();
        $(".store-not-found").hide();
        $(".k__onde-comprar").attr("id", "screen__not-found");
    });


    $(".go-back-container").live("click", function(event) {
        /*Go back With previous version */
        event.stopPropagation();
        $(".k__onde-comprar").attr("id", "screen__default");
    });

    $(".k__onde-comprar-warning").on("click", function(){
        /* Formulario 'Denuncie a Falsificacao' */
        sessionStorage.setItem("subject", "34");        
    });

    $(window).resize(function(){
        if($(window).width() <= 768){
            $(window).on("scroll", function(){
                var scroll = (window.pageYOffset || document.scrollTop);
                if(scroll > 400 && $(window).width() <= 768){
                    $(".k__onde-comprar-warning").fadeOut(200);
                }
                else {
                    $(".k__onde-comprar-warning").fadeIn(200);
                }
            });
        }
    });

    button.onclick = function() {
        searchTypeGeo = false;
        // codeAddress();
        getUserLocation("inputType");
        if (userMarker != undefined) {
            /*Clear previous markers after defined*/
            userMarker.setMap(null);
        }
        $(".k__onde-comprar").attr("id", "screen__search-result");
    };

    onkeypress = function(event) {
        var key = event.which || event.keyCode;
        if (key == 13) {
            //Trigger input button when Enter
            button.onclick();
        }
    };
};

window.onload = function(){
    loadMap();
}