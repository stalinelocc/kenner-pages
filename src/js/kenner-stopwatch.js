var countDownDate = new Date("Dec 19 2019 18:00:00").getTime();
var displayTimerDay = document.querySelector(".timer-day");
var displayTimerHour = document.querySelector(".timer-hour");
var displayTimerMinute = document.querySelector(".timer-minute");
var displayTimerSeconds = document.querySelector(".timer-seconds");
//Update the count down every 1 second
var timer = setInterval(function() {    
    // Get today's date and time
    var now = new Date().getTime();

    // Find the distance between now and the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);    

    // Display the result in the element with id="demo"
    displayTimerDay.innerHTML = ("0" + days).slice(-2) + " : " ;
    displayTimerHour.innerHTML = ("0" + hours).slice(-2) + " : ";
    displayTimerMinute.innerHTML = ("0" + minutes).slice(-2) + " : ";
    displayTimerSeconds.innerHTML = ("0" + seconds).slice(-2);
    
    if (distance < 0) {
        clearInterval(timer);
        endOfTime();
    }
}, 1000);


function endOfTime(){    
    // If the count down is finished, write some text
    var blink = 0;
    
    setInterval(function(){
        blink++;
        console.log("blink", blink)
        if(blink % 2 == 0){
            displayTimer.style.color = "#e92d06";
        } else {
            displayTimer.style.color = "#fff";
        }
    }, 200);
}
